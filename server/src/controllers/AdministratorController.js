const config = require('../config/config')
const admin = require('firebase-admin')
const FieldValue = require('firebase-admin').firestore.FieldValue
// get serviceAccount json file  from config
var db = admin.firestore()

// const user = require('../models/User.js')
// const userData = user.data

module.exports = {
  async index (req, res) {
    function listAllUsers (nextPageToken) {
      // List batch of users, 1000 at a time.
      admin.auth().listUsers(1000, nextPageToken)
        .then(function (listUsersResult) {
          listUsersResult.users.forEach(function(userRecord) {
            console.log("user", userRecord.toJSON())
          })
          if (listUsersResult.pageToken) {
            // List next batch of users.
            listAllUsers(listUsersResult.pageToken)
          }
        })
        .catch(function (error) {
          console.log("Error listing users:", error)
        })
    }
    // Start listing users from the beginning, 1000 at a time.
    listAllUsers();
  }
}
