const User = require('../models/User.js')
// const userData = user.data
const admin = require('firebase-admin')
var db = admin.firestore()

module.exports = {
  async indexUserInfo (req, res) {
    try {
      // create users using Firebase Admin SDK https://firebase.google.com/docs/auth/admin/manage-users
      // user model might be useless on firebase since everything's done by firebase
      admin.auth().getUser(req.body.uid)
      .then(function (userRecord) {
        // See the UserRecord reference doc for the contents of userRecord.
        // get attached user info from users collection
        // var userInfo = Object.assign( userRecord.toJSON(), { registerCompleted: checkUserBasicInfo (req.body.uid) })
        var userInfo = userRecord.toJSON()
        res.send(userInfo)
      })
      .catch(function (error) {
        console.log("Error fetching user data:", error)
      })
    } catch (err) {
      console.log(err)
    }
  },
  async updateUserInfo (req, res) {
    try {
      // create users using Firebase Admin SDK https://firebase.google.com/docs/auth/admin/manage-users
      // user model might be useless on firebase since everything's done by firebase
      User.create(req.body)
      res.send('create user info completed')
    } catch (err) {
      console.log(err)
    }
  },
  async updateUserInfoLicenseImage (req, res) {
    try {
      // create users using Firebase Admin SDK https://firebase.google.com/docs/auth/admin/manage-users
      // user model might be useless on firebase since everything's done by firebase
      var userRef = db.collection('users').doc(req.body.uid)
      // Set the "capital" field of the city 'DC'
      return userRef.update({
          licenseImageDownloadURL: req.body.licenseImageDownloadURL,
          licenseImageVerified: false
      })
      .then(function() {
          console.log("License Image successfully updated!")
      })
      .catch(function(error) {
          // The document probably doesn't exist.
          console.error("Image updating error: ", error)
      })
    } catch (err) {
      console.log(err)
    }
  }
}
