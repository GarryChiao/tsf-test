const config = require('../config/config')
const admin = require('firebase-admin')
const FieldValue = require('firebase-admin').firestore.FieldValue
// get serviceAccount json file  from config
var serviceAccount = require(config.serviceAccount)

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  storageBucket: "gs://tsf-test-5360d.appspot.com"
})
var db = admin.firestore()

// const user = require('../models/User.js')
// const userData = user.data

module.exports = {
  async register (req, res) {
    try {
      // create users using Firebase Admin SDK https://firebase.google.com/docs/auth/admin/manage-users
      // user model might be useless on firebase since everything's done by firebase
      admin.auth().createUser({
        email: req.body.email,
        emailVerified: false,
        phoneNumber: req.body.phoneNumber,
        phoneVerified: false,
        password: req.body.password,
        displayName: req.body.displayName,
        photoURL: 'http://www.example.com/12345678/photo.png',// needs to be changed
        disabled: false
      })
        .then(function(userRecord) {
          // See the UserRecord reference doc for the contents of userRecord.
          console.log("Successfully created new user:", userRecord.uid)
        })
        .catch(function(error) {
          console.log("Error creating new user:", error)
        })
    } catch (err) {
      console.log(err)
    }
  },

  async login (req, res) {
    admin.auth().getUserByEmail(req.body.email)
      .then(function(userRecord) {
        // See the UserRecord reference doc for the contents of userRecord.
        console.log("Successfully fetched user data:", userRecord.toJSON())
      })
      .catch(function(error) {
        console.log("Error fetching user data:", error)
      })
  }
}
