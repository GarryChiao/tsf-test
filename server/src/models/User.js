// since the firebase has been initialized at
const config = require('../config/config')
const admin = require('firebase-admin')
const FieldValue = require('firebase-admin').firestore.FieldValue

var db = admin.firestore()
// validate input data using policies

module.exports = {
  create (userData) {
    try {
      // create users using Firebase Admin SDK https://firebase.google.com/docs/auth/admin/manage-users
      // but this part of information is not defined by firebase authentication
      // so the rest user info stored basically
      var data = {
        realName: userData.realName,
        birthDate: userData.birthDate,
        licenseNumber: userData.licenseNumber,
        licenseType: userData.licenseType,
        address: userData.address,
        phoneNumber: userData.phoneNumber,
        accountReceivable: userData.accountReceivable,
        timestamp: FieldValue.serverTimestamp()
      }
      
      db.collection('users').doc(userData.uid).set(data)
        .then(() => {
          console.log('user updated!')
        })
    } catch (err) {
      console.log(err)
    }
  }
}
