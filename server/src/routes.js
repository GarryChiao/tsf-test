// Authentication Part
const AuthenticationController = require('./controllers/AuthenticationController')
const AuthenticationControllerPolicy = require('./policies/AuthenticationControllerPolicy')
// User Part
const UserController = require('./controllers/UserController')
const UserControllerPolicy = require('./policies/UserControllerPolicy')
// Administrator Part
const Administrator = require('./controllers/AdministratorController')
module.exports = (app) => {
  // Authentication
  app.post('/register',
    AuthenticationControllerPolicy.register,
    AuthenticationController.register
  )
  app.post('/login',
    AuthenticationController.login
  )
  // User
  app.post('/indexUserInfo',
    UserController.indexUserInfo
  ) // use the same one to create and update
  app.post('/updateUserInfo',
    UserControllerPolicy.updateUserInfo,
    UserController.updateUserInfo
  )
  app.post('/updateUserInfoLicenseImage',
    UserController.updateUserInfoLicenseImage
  )
  // Administrator
  app.post('/admin',
    Administrator.index
  )
}
