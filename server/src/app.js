const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')// shows restful request details
const config = require('./config/config')

const app = express()
app.use(morgan('combined'))
app.use(bodyParser.json({limit: '50mb'}))
app.use(cors())

require('./routes')(app)

app.listen(config.port, function () {
  console.log('app running on port ', config.port)
})
