const Joi = require('joi')
// Joi is to verify inputs
module.exports = {
  updateUserInfo (req, res, next){
    const schema = Joi.object().keys({
      realName: Joi.string(),
      birthDate: Joi.date(),
      licenseNumber: Joi.any(),
      licenseType: Joi.any(),
      address: Joi.any(),
      phoneNumber: Joi.any(),
      accountReceivable: Joi.any(),
      uid: Joi.any()
    })

    const {error, value} = Joi.validate(req.body, schema)

    if(error){
      switch (error.details[0].context.key) {
        case 'realName':
          res.status(400).send({
            error: 'Your real name should be a string'
          })
          break
        case 'birthDate':
          res.status(400).send({
            error: 'Birth Date Error'
          })
          break
        default:
          res.status(400).send({
            error: 'Invalid User information.'
          })
      }
    }else {
      next()
    }
  }
}
