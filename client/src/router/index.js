import Vue from 'vue'
import Router from 'vue-router'
import firebase from 'firebase'
// homepage
import HelloWorld from '@/components/HelloWorld'
// entrance
import Register from '@/components/entrance/Register'
import SignIn from '@/components/entrance/SignIn'
// users
import UserIndex from '@/components/users/Index'
import UserUpdate from '@/components/users/Update'

Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '*',
      redirect: '/'
    },
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld,
      meta: {
        requiresAuth: true
      }
    }, // entrance
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/signIn',
      name: 'SignIn',
      component: SignIn
    }, // user info
    {
      path: '/user',
      name: 'UserIndex',
      component: UserIndex,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/user/update',
      name: 'UserUpdate',
      component: UserUpdate,
      meta: {
        requiresAuth: true
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  let currentUser = firebase.auth().currentUser
  let requiresAuth = to.matched.some(record => record.meta.requiresAuth)

  if (requiresAuth && !currentUser) {
    next('signIn')
  } else if (!requiresAuth && currentUser) {
    next('/')
  } else {
    next()
  }
})

export default router
