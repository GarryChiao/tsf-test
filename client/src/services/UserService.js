import Api from '@/services/Api'

export default {
  indexUserInfo (credentials) {
    return Api().post('indexUserInfo', credentials)
  },
  updateUserInfo (credentials) {
    return Api().post('updateUserInfo', credentials)
  },
  updateUserInfoLicenseImage (credentials) {
    return Api().post('updateUserInfoLicenseImage', credentials)
  }
}
